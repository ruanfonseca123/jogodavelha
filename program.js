permissao = 0;
var placar = [0,0];
vez = 1;
vai = 0;
casael = document.getElementById('casa'+1).src;
contador = 0;
var jogotab = [[],[]];
var venpos = [0,0];
/* Abaixo o vetor com todas as possibilidades possíveis, para serem tratadas na função "ganhou"
Cada linha representa as possibilidades de vitória de cada casa*/
var possibilidades = [[ [ 1, 2, 3 ], [ 1, 4, 7 ], [ 1, 5, 9 ] ],
		[ [ 1, 2, 3 ], [ 2, 5, 8 ] ],
		[ [ 1, 2, 3 ], [ 3, 6, 9 ], [ 3, 5, 7 ] ],
		[ [ 4, 5, 6 ], [ 1, 4, 7 ] ],
		[ [ 4, 5, 6 ], [ 2, 5, 8 ], [ 1, 5, 9 ], [ 3, 5, 7 ] ],
		[ [ 4, 5, 6 ], [ 3, 6, 9 ] ],
		[ [ 7, 8, 9 ], [ 1, 4, 7 ], [ 3, 5, 7 ] ],
		[ [ 7, 8, 9 ], [ 2, 5, 8 ] ],
		[ [ 7, 8, 9 ], [ 3, 6, 9 ], [ 1, 5, 9 ] ]];

function menu(op) { /*Controle dos botões de ação do Navbar*/
	if (op == 3){
		botao = document.getElementById('botao3');
		if (permissao == 0 && botao.value != "Cancelar Partida"){
			permissao = 1;
			botao.value = "Cancelar Partida";
			botao.className = "btn btn-danger";
			botaoaux = document.getElementById('botao2');
			botaoaux.value = "Zerar placar";
			botaoaux.className = "btn btn-warning";
			infovez = document.getElementById('vez');
			infovez.style.fontSize = '20px';
			infovez.innerHTML = "Vez de: <img class='ajuste img-fluid' src='img/papaleguas.gif'>";
		}else if(permissao == 1 || 	botao.value == "Cancelar Partida"){
			permissao = 0;
			botao.value = "2 Jogadores";
			botao.className = "btn btn-success";
			botaoaux = document.getElementById('botao2');
			botaoaux.value = "Versus CPU";
			botaoaux.className = "btn btn-primary";
			infovez = document.getElementById('vez');
			infovez.style.fontSize = '30px';
			infovez.innerHTML = "<strong>COIOTE X PAPA-LÉGUAS</strong><br>Perseguição selvagem!";
			resetar(0);	
		}

	}else if (op == 2) {
		botao0 = document.getElementById('botao2');
		if (botao0.value == "Versus CPU"){
			infovez = document.getElementById('infor');
			infovez.className = "alert alert-danger";
			if(infovez.innerHTML == ""){
				infovez.innerHTML += "<strong>Erro! </strong>Função indisponível no momento.";
			}
		}else{
			placar = [0,0];
			plac = document.getElementById('placar');
			plac.innerHTML = '<img class="casa img-fluid" src="img/1.png"><strong>'+placar[0]+'</strong> x <strong>'+placar[1]+'</strong><img class="casa img-fluid"src="img/2.png">'

		}
	}
}
function jogar(cs) {
	casa = document.getElementById('casa'+cs);
	if(casa.src === casael && (permissao == 1 || vai == 1)){
		if (vai == 1){
			permissao = 1;
			vai = 0;
		}
		contador += 1;
		casa.src = "img/"+vez+".png";
		infovez = document.getElementById('infor');
		infovez.className = "";
		infovez.innerHTML = "";
		jogotab[vez-1].push(cs);
		if(vez == 1){
			infovez = document.getElementById('vez');
			infovez.innerHTML = "Vez de: <img class='ajuste img-fluid' src='img/coiote.gif'>";
			vezt = vez;
			vez = 2;
		}else{
			infovez = document.getElementById('vez');
			infovez.innerHTML = "Vez de: <img class='ajuste img-fluid' src='img/papaleguas.gif'>";
			vezt = vez;
			vez = 1;
		}
		if(contador >= 5){
			resultado = ganhou(cs);
			console.log(resultado);
			if (resultado == "alguem ganhou"){
				permissao = 0;
				tabelinha = possibilidades[venpos[0]][venpos[1]];
				for (var i = 0; i < 3; i++) {
					casacompleta = document.getElementById('casa'+tabelinha[i]);
					casacompleta.style.backgroundColor = 'orange';
				}
				if (vezt == 1){
					res = document.getElementById('infor');
					res.className = "alert alert-warning";
					res.innerHTML = "O PAPA-LÉGUAS VENCEU!";
					infovez = document.getElementById('vez');
					infovez.innerHTML = "<strong>BIP-BIP!!! </strong><img class='ajuste img-fluid' src='img/pvence.gif'>  <input type='button' class='btn btn-warning' value='Continuar' onclick='resetar(1)'>";
					placar[0] += 1;	
				}else if (vezt == 2){
					res = document.getElementById('infor');
					res.className = "alert alert-warning";
					res.innerHTML = "Wilie E. Coiote VENCEU!";
					infovez = document.getElementById('vez');
					infovez.innerHTML = "<strong>GENIUS!!! </strong><img class='ajuste img-fluid' src='img/cvence.gif'>  <input type='button' class='btn btn-warning' value='Continuar' onclick='resetar(1)'>";
					placar[1] += 1;	
				}		
			}
		}
		if (contador >= 9 && permissao == 1){
			res = document.getElementById('infor');
			res.className = "alert alert-primary";
			res.innerHTML = "Deu Velha!!!";
			infovez = document.getElementById('vez');
			infovez.innerHTML = "<strong>Tente de novo!!! </strong><img class='ajuste img-fluid' src='img/velha.gif'>  <input type='button' class='btn btn-warning' value='Continuar' onclick='resetar(1)'>";
					
		}

	}else{
		infovez = document.getElementById('infor');
		if(infovez.innerHTML == "" && permissao == 1){
			infovez.className = "alert alert-danger";
			infovez.innerHTML += "<strong>Erro! </strong>Casa já ocupada.";
		}
	}
}
/* Abaixo a função "resetar" que reseta as casas para o padrão inicial
Usada no controle dos botões do Navbar e no botão "continuar", que aparece logo após o fim da partida*/
function resetar(opr){
	for (var i = 1; i <= 9; i++) {
		casa = document.getElementById('casa'+i)
		casa.src = 'img/fundo.png';
		casa.style.backgroundColor = '';
	}
	contador = 0;
	if (opr == 1){
		vai = 1;
		infovez = document.getElementById('vez');
		infovez.style.fontSize = '20px';
		infovez.innerHTML = "Vez de: <img class='ajuste img-fluid' src='img/papaleguas.gif'>";
	}
	vez = 1;
	jogotab = [[],[]];
    venpos = [0,0];
	infoveza = document.getElementById('infor');
	infoveza.className = "";
	infoveza.innerHTML = "";
	tab = document.getElementById('tabuleiro');
	plac = document.getElementById('placar');
	plac.innerHTML = '<img class="casa img-fluid" src="img/1.png"><strong>'+placar[0]+'</strong> x <strong>'+placar[1]+'</strong><img class="casa img-fluid"src="img/2.png">'

}
/* Abaixo a função "ganhou" que verifica se alguém ganhou. O ganhador é verificado lá em cima,
 a partir da vez de quem jogou por último*/
function ganhou(casa){
	testador = possibilidades[casa-1];
	console.log(jogotab);
	for (var i = 0; i < testador.length; i++) {
		if(jogotab[vezt-1].includes(testador[i][0]) == true && jogotab[vezt-1].includes(testador[i][1]) == true && 
			jogotab[vezt-1].includes(testador[i][2]) == true){
			venpos[0] = casa-1;
			venpos[1] = i;
			return "alguem ganhou";
		}
	}
	return "ninguem ganhou";
}
